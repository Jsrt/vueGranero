$(document).ready(function(){
	$('select').formSelect();
    $('.modal').modal();
	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy',
		i18n: {
			months: 	
			[
			'Enero',
			'Febrero',
			'Marzo',
			'Abril',
			'Mayo',
			'Junio',
			'Julio',
			'Agosto',
			'Septiembre',
			'Octubre',
			'Noviembre',
			'Deciembre'
			],
			monthsShort:
			['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
			weekdaysShort:	
			['Sab','Lun','Mar','Mie','Jue','Vie','Dom'],
			done: 'Aceptar',
			cancel: 'Cancelar'
		}
	});		  
  });
const app = new Vue({
	el: 'main',
	data: {
		titleHome: 'Granero Pague Menos',
		titleUser: 'Usuarios',
		usuarios: [],
		productos: [],
		ventas: [],
		selProds: [],
		unidades: [
		{'unidad': 'unidad(es)'},
		{'unidad': 'gramo(s)'},
		{'unidad': 'libra(s)'},
		{'unidad': 'kilo(s)'},
		{'unidad': 'mililitro(s)'},
		{'unidad': 'litro(s)'}
		],
		frmProducto: {},
		acciones: 'crear',
		showDeleteModal: false,
		frmVenta: {},
		frmInven: {},
		inventarios: [],
		idProd: '',
		idInve: ''
	},
	mounted() {
		this.searchSelProd()
		this.acciones = 'crear'
	},
	methods: {
		getProducto(datos){
			if(typeof datos == 'object')
			{
				this.frmProducto = datos
				this.acciones = 'editar'	
			}else{
				this.acciones = 'eliminar'
				this.idProd = datos
			}
		},
		getVentas(datos){
			if(Object.keys(datos).length > 1)
			{
				this.frmVenta = datos
				this.acciones = 'editar'	
			}
		},
		getInventario(datos){
			if(typeof datos == 'object')
			{
				this.frmInven = datos
				this.acciones = 'editar'	
			}else{
				this.acciones = 'eliminar'
				this.idInve = datos
			}
		},
		actionVent(accion,e){
			var frmvent = document.getElementById('ventas')
			switch(accion){
				case 'crear':
				axios.post('../api/transaccion.php?entidad=ventas', new FormData(frmvent))
				.then(response => {
					document.querySelector("form").reset()
					alert(response.data)
					this.searchVent()
				})
				break;
				case 'editar':
				axios.post('../api/transaccion.php?entidad=update_vent&&id_venta='+this.frmVenta.id_venta, new FormData(frmvent))
				.then(response => {
					alert(response.data)
						this.searchVent()
						frmvent.reset()
						this.frmVenta = {}
						acciones = 'crear'
				})
				break;
			}
		},
		actionProd(accion,e){
		var frmProd = document.getElementById('productos')
		switch(accion){
			case 'crear':
			axios.post('api/transaccion.php?entidad=productos', new FormData(frmProd))
			.then(response => {
				alert(response.data)
				this.searchProd()
				frmProd.reset()
				this.frmProducto = {}
			})
			break;
			case 'editar':
			axios.post('api/transaccion.php?entidad=update_prod&&id_producto='+this.frmProducto.id_producto, new FormData(frmProd))
			.then(response => {
				alert(response.data)
					this.searchProd()
					frmProd.reset()
					this.frmProducto = {}
					acciones = 'crear'
			})
			break;
			case 'eliminar':
				axios.get('api/transaccion.php?entidad=delete_prod&&id_producto='+this.idProd)
        		.then(response => {
	             	alert(response.data)
	             	this.searchProd()
	            } )
			break;
			}
		},
		actionInv(accion,e){
		var frmInv = document.getElementById('inventarios')
		switch(accion){
			case 'crear':
			axios.post('../api/transaccion.php?entidad=inventarios', new FormData(frmInv))
			.then(response => {
				alert(response.data)
				this.searchInv()
				frmInv.reset()
				this.frmInven = {}
			})
			break;
			case 'editar':
			axios.post('../api/transaccion.php?entidad=update_inve&&id_inventario='+this.frmInven.id_inventario, new FormData(frmInv))
			.then(response => {
				alert(response.data)
					this.searchInv()
					frmInv.reset()
					this.frmInven = {}
					acciones = 'crear'
			})
			break;
			case 'eliminar':
				axios.post('api/transaccion.php?entidad=delete_prod', {id:id_producto})
        		.then(response => {
	             
	            } )
			break;
			}
		},
		searchProd(){
			axios.get('api/transaccion.php',
			{
				params:{
					entidad: 'productos'
				}
			})
			.then(response => {
				this.productos = response.data.prod
			}).catch(function(response){
				console.error(response)
			})
		},
		searchVent(){
			axios.get('../api/transaccion.php',
			{
				params:{
					entidad: 'ventas'
				}
			})
			.then(response => {
				this.ventas = response.data.vent
			}).catch(function(response){
				console.error(response)
			})	
		},
		searchInv(){
			axios.get('../api/transaccion.php',
			{
				params:{
					entidad: 'inventarios'
				}
			})
			.then(response => {
				this.inventarios = response.data.inven
			})
		},
		searchSelProd(){
			axios.get('../api/transaccion.php',
			{
				params:{
					entidad: 'selProd'
				}
			})
			.then(response => {
				this.selProds = response.data.selProd
			})
		},
		createdVent(e){
			axios.post('../api/transaccion.php?entidad=ventas', new FormData(e.target))
			.then(response => {
				document.querySelector("form").reset()
				alert(response.data)
				this.searchVent()
			})
			.catch(function(response){
				console.error(response)
			})
		}
	}
})