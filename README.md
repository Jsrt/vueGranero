descripcion del proyecto

Estructura del proyecto
Api: carpeta donde se encuentran dos archivos php que hacen referencia a la conexión y transacciones que se realizan en la aplicación.

conexion.php: Se encuentra la conexión a la base de datos

Transaccion.php:  se encuentran los metodos de todas las acciones que se pueden realizar en el aplicativo, como son guardar, eliminar, modificar.


app: en la carpeta app encontrará un archivo js, en donde se realizan las peticiones, se crean las variables utilizando vue,js. Aqui esta toda la logica. Se definen los nombres de
de los metodos. 

Librarys: En esta carpeta se encuentra la libreria Materialize. En la aplicación tambien se utilizarón otras librerias como vue.js, axios y jquery.
Axios(libreria para hacer peticiones).

Jquery( se agrego para poder inicializar algunos elementos del materialize como el clock piquer y la opción de select)

pages: en pages encontraran todas las paginas que conforman la aplicación. 

Config: Es un archivo de tipo de configuración donde estan los parametros de la base de datso, como son el hots, el nombre de usuario, contraseña (que en este caso no tiene),
el nombre de la base de datos.

Index: Es la pagina principal del aplicativo.